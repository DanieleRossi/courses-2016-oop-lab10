package it.unibo.oop.lab.workers02;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * This is a standard implementation of the calculation.
 * 
 */

public class MultiThreadMatrixSum implements SumMatrix {

    private final int nthread;

    /**
     * 
     * @param nthread
     *            no. of thread performing the sum.
     */
    public MultiThreadMatrixSum(final int nthread) {
        this.nthread = nthread;
    }

    private static class Worker extends Thread {
        private final double[][] matrix;
        private final int startpos;
        private final int nrow;
        private long res;
        final private int ncol;

        /**
         * Build a new worker.
         * 
         * @param list
         *            the list to sum
         * @param startpos
         *            the initial position for this worker
         * @param nelem
         *            the no. of elems to sum up for this worker
         */
        Worker(final double[][] matrix, final int startpos, final int nrow) {
            super();
            this.matrix = matrix;
            this.startpos = startpos;
            this.nrow = nrow;
            ncol = matrix[0].length;
        }

        @Override
        public void run() {
            System.out.println("Working from row " + startpos + " to row " + (startpos + nrow));
            for (int k = startpos; k < startpos + nrow && k < matrix.length; k++) {
                for (int i = 0; i < ncol; i++) {
                    res += matrix[k][i];
                }
            }
        }

        /**
         * Returns the result of summing up the integers within the list.
         * 
         * @return the sum of every element in the array
         */
        public long getResult() {
            return this.res;
        }

    }

    public double sum(double[][] matrix) {
        /*
         * Build a list of workers
         */
        final int nrow = (matrix.length) / nthread + (matrix.length) % nthread;
        final List<Worker> workers = IntStream.iterate(0, start -> start + nrow).limit(nthread)
                .mapToObj(start -> new Worker(matrix, start, nrow)).collect(Collectors.toList());
        /*
         * Start them
         */
        workers.forEach(Thread::start);
        /*
         * Wait for every one of them to finish
         */
        workers.forEach(t -> {
            try {
                t.join();
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
        });
        /*
         * Return the sum
         */
        return workers.stream().mapToDouble(Worker::getResult).sum();
    }
}
