package it.unibo.oop.lab.reactivegui03;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.event.TreeWillExpandListener;

public final class AnotherConcurrentGUI extends JFrame {
    private static final long serialVersionUID = 1L;
    private static final double WIDTH_PERC = 0.2;
    private static final double HEIGHT_PERC = 0.1;
    private final JLabel display = new JLabel();
    private final JButton stop = new JButton("stop");
    private final JButton up = new JButton("up");
    private final JButton down = new JButton("down");
    private final Agent agent = new Agent();

    /**
     * 
     */
    public AnotherConcurrentGUI() {
        super();
        final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setSize((int) (screenSize.getWidth() * WIDTH_PERC), (int) (screenSize.getHeight() * HEIGHT_PERC));
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        final JPanel panel = new JPanel();
        panel.add(display);
        panel.add(stop);
        panel.add(up);
        panel.add(down);
        this.getContentPane().add(panel);
        this.setVisible(true);
        new Thread(agent).start();
        new Thread(new TimeControll()).start();
        /*
         * Register a listener that stops it
         */
        stop.addActionListener(e-> agent.stopCounting());
        up.addActionListener(e -> agent.setDirection(true));
        down.addActionListener(e -> agent.setDirection(false));
    }

    private class Agent implements Runnable {
        /*
         * stop is volatile to ensure ordered access
         */
        private volatile boolean stop;
        private volatile boolean direction = true;
        private int counter;

        public void run() {
            while (!this.stop) {
                try {
                    /*
                     * All the operations on the GUI must be performed by the
                     * Event-Dispatch Thread (EDT)!
                     */
                    SwingUtilities.invokeAndWait(new Runnable() {
                        public void run() {
                            AnotherConcurrentGUI.this.display.setText(Integer.toString(Agent.this.counter));
                        }
                    });
                    if (direction) {
                        this.counter++;
                    } else {
                        this.counter--;
                    }
                    Thread.sleep(100);
                } catch (InvocationTargetException | InterruptedException ex) {
                    /*
                     * This is just a stack trace print, in a real program there
                     * should be some logging and decent error reporting
                     */
                    ex.printStackTrace();
                }
            }
        }

        protected void setDirection(final boolean val) {
            this.direction = val;
        }

        /**
         * Excternal command to stop counting.
         */
        public void stopCounting() {
            this.stop = true;
            AnotherConcurrentGUI.this.down.setEnabled(false);
            AnotherConcurrentGUI.this.up.setEnabled(false);
            AnotherConcurrentGUI.this.stop.setEnabled(false);
        }
    }

    private class TimeControll implements Runnable {
        private static final int ATTENDI = 10990;
        public void run() {
            try {
                Thread.sleep(ATTENDI);
                SwingUtilities.invokeAndWait(new Runnable() {
                    public void run() {
                        AnotherConcurrentGUI.this.stop.setEnabled(false);
                        AnotherConcurrentGUI.this.down.setEnabled(false);
                        AnotherConcurrentGUI.this.up.setEnabled(false);
                        AnotherConcurrentGUI.this.agent.stopCounting();
                    }
                });

            } catch (InterruptedException | InvocationTargetException e) {
                e.printStackTrace();
            }

        }

    }
}
